package se.experis.jokeGenerator.Controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import se.experis.jokeGenerator.JsonStuff.getApi;
import org.springframework.stereotype.Controller;

@Controller("/")
public class homepageController implements ErrorController {

    String joke = "";
    String returnJoke = "";
    String jokeImage = "spiderman.jpg";


    @GetMapping("/")
    public String joke(Model model){
        model.addAttribute("jokeImage",jokeImage);
        if(!joke.equals("")){

            model.addAttribute("joke",returnJoke);
            joke = "";
            jokeImage = "spiderman.jpg";
        }
        return "homePage";
    }

    @PostMapping("/")
    public String addJoke(String getJoke) throws Exception {
        joke = getJoke;
        getApi api = new getApi();
        if(getJoke.equals("GetChuckNorrisJoke")){
           returnJoke = api.getChuckJoke("https://api.chucknorris.io/jokes/random");
           jokeImage = "Chuck.jpg";
        } else if (getJoke.equals("GetDadJoke")){
            returnJoke = api.getDadJoke("https://icanhazdadjoke.com/slack");
            jokeImage = "dad.jpg";
        } else if (getJoke.equals("GetProgrammerJoke")){
            returnJoke = api.getProgrammerJoke("https://official-joke-api.appspot.com/jokes/programming/random");
            jokeImage = "programmer.jpg";
        }

        return "redirect:/";
    }

    @RequestMapping("/error")
    public String handleError() {
        jokeImage = "spiderman.jpg";
        return "redirect:/";
    }
    @Override
    public String getErrorPath() {
        return "/error";
    }
}
