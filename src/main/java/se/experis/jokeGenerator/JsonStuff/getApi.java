package se.experis.jokeGenerator.JsonStuff;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/*
    rjanul created on 2019-10-01
*/
public class getApi {

    private String requestURL(String url) throws Exception{
        // Set URL
        URLConnection connection = new URL(url).openConnection();
        // Set property - avoid 403 (CORS)
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        // Create connection
        connection.connect();
        // Create a buffer of the input
        BufferedReader buffer  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        // Convert the response into a single string
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = buffer.readLine()) != null) {
            stringBuilder.append(line);
        }
        // return the response
        return stringBuilder.toString();
    }

    public String getProgrammerJoke(String url) throws Exception {
        JSONArray joArray = new JSONArray(requestURL(url));
        JSONObject jo = joArray.getJSONObject(0);

        String setup = jo.getString("setup");
        String punchLine = jo.getString("punchline");

        return setup + "<br>" + punchLine;
    }

    public String getChuckJoke(String url) throws Exception {
        JSONObject jo = new JSONObject(requestURL(url));
        String value = jo.getString("value");
        return value;
    }

    public String getDadJoke(String url) throws Exception {
        JSONObject jo = new JSONObject(requestURL(url));
        JSONArray joArray = new JSONArray(jo.get("attachments").toString());
        JSONObject joAgain = joArray.getJSONObject(0);
        String text = joAgain.getString("text");
        return text;
    }

}
